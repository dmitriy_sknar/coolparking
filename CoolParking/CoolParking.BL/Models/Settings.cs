﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

namespace CoolParking.BL.Models {
    public static class Settings {
        public static readonly decimal INITIAL_BALANCE = 0;
        public static readonly int PARKING_CAPACITY = 10;
        public static readonly int CHARGE_TIMEOUT = 10 * 1000;
        public static readonly int LOG_TIMEOUT = 60 * 1000;
        public static readonly decimal FINE_MULTIPLIER = 2.5m;
        public static readonly decimal BUS_RATE = 3.5m;
        public static readonly decimal TRUCK_RATE = 5m;
        public static readonly decimal PASSENGERCAR_RATE = 2m;
        public static readonly decimal MOTORCYCLE_RATE = 1m;
        public static readonly string HOST = "http://localhost:8266/";
    }
}