﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CoolParking.BL.Models {
    public class Vehicle {
        [JsonProperty("Balance")]
        public decimal Balance { get; set; }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; set; }
        private static Random _random = new Random();

        public Vehicle() { }

        public Vehicle(string id, VehicleType type, decimal balance) {
            if (IsBadId(id))
                throw new ArgumentException("Vehicle Id is not valid");
            if (balance < 0)
                throw new ArgumentException("Vehicle Balance is not valid");
            if ((int)type < 1 || (int)type > 4)
                throw new ArgumentException("Vehicle Id is not valid");

            this.Balance = balance;
            this.VehicleType = type;
            this.Id = id;
        }

        public static bool IsBadId(string input) {
            if (string.IsNullOrEmpty(input)) return true;
            Regex regex = new Regex(@"\w{2}-\d{4}-\w{2}");
            MatchCollection matches = regex.Matches(input);
            if (matches.Count > 0) return false;
            return true;
        }

        public static string GenerateRandomRegistrationPlateNumber() {
            return RandomString(2, false)
                   + "-"
                   + RandomString(4, true)
                   + "-"
                   + RandomString(2, false);
        }

        public static string RandomString(int length, bool isNumbers) {
            const string letterChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string numberChars = "0123456789";
            string chars = isNumbers ? numberChars : letterChars;
            return new string(Enumerable.Repeat(chars, length).Select(s => s[_random.Next(s.Length)]).ToArray());
        }
    }
}