﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.

namespace CoolParking.BL.Models {
    public enum VehicleType {
        Bus = 1,
        Truck = 2,
        PassengerCar = 3,
        Motorcycle = 4
    }
}