﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CoolParking.BL.Models {
    public struct TransactionInfo {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        [JsonProperty("transactionDate")]
        public DateTime TransactionTime { get; set; }

        public override string ToString() {
            return $"{nameof(VehicleId)}: {VehicleId}, {nameof(Sum)}: {Sum}, {nameof(TransactionTime)}: {TransactionTime}";
        }
    }

    public static class TransactionInfoUtils {
        public static decimal Sum(this TransactionInfo[] transactionInfos) {
            decimal total = 0;
            foreach (var transactionInfo in transactionInfos) {
                total += transactionInfo.Sum;
            }

            return total;
        }
    }
}