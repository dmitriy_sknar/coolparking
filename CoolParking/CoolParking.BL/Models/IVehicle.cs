﻿namespace CoolParking.BL.Models {
    public interface IVehicle {
        public decimal Balance { get; set; }
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
    }
}