﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models {
    public sealed class Parking {
        public decimal ParkingBalance { get; set; }
        public Dictionary<string, Vehicle> Places;

        public static Parking Instance {
            get { return _instance.Value; }
        }

        private static readonly Lazy<Parking> _instance =
            new Lazy<Parking>(() => new Parking {
                Places = new Dictionary<string, Vehicle>(),
                ParkingBalance = Settings.INITIAL_BALANCE
            });
    }
}