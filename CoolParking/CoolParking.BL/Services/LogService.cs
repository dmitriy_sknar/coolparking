﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services {
    public class LogService : ILogService, IDisposable {
        public string LogPath { get; }

        public LogService(string logFilePath) {
            LogPath = logFilePath;
            if (File.Exists(LogPath)) {
                File.Delete(LogPath);
                using var file = new StreamWriter(LogPath, false);
                file.Write("");
            }
            if (!File.Exists(LogPath)) {
                using var file = new StreamWriter(LogPath, false);
                file.Write("");
            }
        }

        public void Write(string logInfo) {
            if (File.Exists(LogPath)) {
                using var file = new StreamWriter(LogPath, true);
                file.WriteLine(logInfo);
            } else {
                using var file = new StreamWriter(LogPath, false);
                file.WriteLine(logInfo);
            }
        }

        public string Read() {
            if (!File.Exists(LogPath)) {
                throw new InvalidOperationException();
            }

            // Open the file to read from.
            using var file = new StreamReader(LogPath);
            return file.ReadToEnd();
        }

        public void Dispose() {
            File.Delete(LogPath);
        }
    }
}