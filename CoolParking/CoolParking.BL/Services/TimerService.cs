﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using System.Timers;
using CoolParking.BL.Interfaces;
using Timer = System.Timers.Timer;

namespace CoolParking.BL.Services {
    public class TimerService : ITimerService {

        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }
        private readonly Timer _timer;

        public TimerService() {
            Interval = 2000;
            _timer = new Timer(Interval);
            _timer.Elapsed += OnTimedEvent;
        }

        public TimerService(Timer timer) {
            _timer = timer;
            _timer.Elapsed += OnTimedEvent;
        }

        public TimerService(double interval) {
            Interval = interval;
            _timer = new Timer(Interval);
            _timer.Elapsed += OnTimedEvent;
        }


        public void Start() {
            _timer.Start();
        }

        public void Stop() {
            _timer.Stop();
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e) {
            Elapsed?.Invoke(source, e);
            // Console.WriteLine("The Elapsed event was raised at {0:HH:mm:ss.fff}", e.SignalTime);
        }

        public void Dispose() {
            _timer.Dispose();
        }
    }
}