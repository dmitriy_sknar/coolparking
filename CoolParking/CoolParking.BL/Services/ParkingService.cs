﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services {
    public sealed class ParkingService : IParkingService {
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;
        private Parking _parking = Parking.Instance;
        private List<TransactionInfo> _transactionsInfo = new List<TransactionInfo>();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService) {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            SubscribeToEvents();
            StartTimers();
        }

        public ParkingService() : this(new TimerService(Settings.CHARGE_TIMEOUT),
            new TimerService(Settings.LOG_TIMEOUT),
            new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log")) { }

        private void StartTimers() {
            _withdrawTimer.Start();
            _logTimer.Start();
        }

        private void SubscribeToEvents() {
            _withdrawTimer.Elapsed -= Charge;
            _withdrawTimer.Elapsed += Charge;
            _logTimer.Elapsed -= LogTransactions;
            _logTimer.Elapsed += LogTransactions;
        }

        private void UnSubscribeToEvents() {
            _withdrawTimer.Elapsed -= Charge;
            _logTimer.Elapsed -= LogTransactions;
        }

        public void Dispose() {
            UnSubscribeToEvents();
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _parking.Places.Clear();
            _parking.ParkingBalance = 0;
            _parking = null;
        }

        public decimal GetBalance() {
            return _parking.ParkingBalance;
        }

        public int GetCapacity() {
            return Settings.PARKING_CAPACITY;
        }

        public int GetFreePlaces() {
            return Settings.PARKING_CAPACITY - _parking.Places.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles() {
            return new ReadOnlyCollection<Vehicle>(_parking.Places.Values.ToList());
        }

        public Vehicle GetVehicle(string id) {
            return _parking.Places.Values.ToList().FirstOrDefault(vehicle => vehicle.Id.Equals(id));
        }

        public void AddVehicle(Vehicle vehicle) {
            if (_parking.Places.Count >= Settings.PARKING_CAPACITY) {
                throw new InvalidOperationException("CoolParking is full. Cannot park vehicle");
            }

            if (_parking.Places.ContainsKey(vehicle.Id)) {
                throw new ArgumentException("Vehicle with same Id cannot be parked two times");
            }

            _parking.Places.Add(vehicle.Id, vehicle);
        }

        public void RemoveVehicle(string vehicleId) {
            if (!_parking.Places.ContainsKey(vehicleId)) {
                throw new ArgumentException("Vehicle cannot be removed from parking as it was not parked");
            }

            if (_parking.Places[vehicleId].Balance >= 0) {
                _parking.Places.Remove(vehicleId);
            } else {
                throw new InvalidOperationException("Vehicle cannot be removed from parking as it has charge debt");
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum) {
            if (sum < 0) {
                throw new ArgumentException("Replenishment cannot be negative");
            }

            if (!_parking.Places.ContainsKey(vehicleId)) {
                throw new ArgumentException("Cannot replenish balance of the vehicle which is not parked");
            }

            _parking.Places[vehicleId].Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions() {
            return _transactionsInfo.ToArray();
        }

        public string ReadFromLog() {
            return _logService.Read();
        }

        public void Charge(object sender, EventArgs args) {
            foreach (Vehicle vehicle in _parking.Places.Values) {
                var vehicleCharge = VehicleCharge(vehicle);
                vehicle.Balance -= vehicleCharge;
                _parking.ParkingBalance += vehicleCharge;
                _transactionsInfo.Add(new TransactionInfo {
                    VehicleId = vehicle.Id,
                    Sum = vehicleCharge,
                    TransactionTime = DateTime.Now
                });
            }
        }

        private decimal VehicleCharge(Vehicle vehicle) {
            decimal fineMultiplier = 1;
            decimal charge;

            switch (vehicle.VehicleType) {
                case VehicleType.Bus:
                    charge = Settings.BUS_RATE;
                    break;
                case VehicleType.Motorcycle:
                    charge = Settings.MOTORCYCLE_RATE;
                    break;
                case VehicleType.PassengerCar:
                    charge = Settings.PASSENGERCAR_RATE;
                    break;
                case VehicleType.Truck:
                    charge = Settings.TRUCK_RATE;
                    break;
                default:
                    throw new ArgumentException("Vehicle type is not supported");
            }

            if (vehicle.Balance - charge >= 0) return charge;
            if (vehicle.Balance <= 0) return charge * Settings.FINE_MULTIPLIER;

            var fineRatePart = charge - vehicle.Balance;
            var normalRatePart = charge - fineRatePart;
            return normalRatePart + fineMultiplier * Settings.FINE_MULTIPLIER;
        }

        private void LogTransactions(object sender, EventArgs args) {
            if (_transactionsInfo.Count != 0) {
                foreach (var transactionInfo in _transactionsInfo.ToArray()) {
                    _logService.Write(transactionInfo.ToString());
                }

                _transactionsInfo.Clear();
            } else {
                _logService.Write("No transactions were made for last period");
            }
        }
    }
}