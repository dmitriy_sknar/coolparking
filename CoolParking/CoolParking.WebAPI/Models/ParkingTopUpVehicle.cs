﻿using Newtonsoft.Json;

namespace CoolParking.WebAPI.Models {
    public class ParkingTopUpVehicle {
        public ParkingTopUpVehicle(string id, decimal sum) {
            Id = id;
            Sum = sum;
        }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
    }
}