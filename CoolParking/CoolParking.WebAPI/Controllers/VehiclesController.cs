﻿using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers {
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class VehiclesController : Controller {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService) {
            _parkingService = parkingService;
        }

        // GET api/vehicles
        //
        // Response:
        // If request is handled successfully
        // Status Code: 200 OK
        // Body schema: [{ “id”: string, “vehicleType”: int, "Balance": decimal
        //     }]
        // Body example: [{ “id”: “GP-5263-GC”, “vehicleType”: 2, "Balance": 196.5 }]
        [HttpGet]
        public JsonResult GetVehicles() {
            var parkedVehicles = _parkingService.GetVehicles();
            return Json(parkedVehicles);
        }

        // GET api/vehicles/id(id - vehicle id of format “AA-0001-AA”)
        //
        // Response:
        // If id is invalid - Status Code: 400 Bad Request
        // If vehicle not found - Status Code: 404 Not Found
        // If request is handled successfully
        // Status Code: 200 OK
        // Body schema: { “id”: string, “vehicleType”: int, "Balance": decimal }
        // Body example: { “id”: “GP-5263-GC”, “vehicleType”: 2, "Balance": 196.5 }
        [HttpGet("{id}")]
        public IActionResult GetVehicle(string id) {
            if (Vehicle.IsBadId(id)) {
                return BadRequest($"Bad request for vehicle with id: {id}");
            }

            var result = _parkingService.GetVehicle(id);
            if (result == null) {
                return NotFound($"Not found vehicle with id: {id}");
            }

            return Json(result);
        }

        // POST api/vehicles
        //
        // Request:
        // Body schema: { “id”: string, “vehicleType”: int, “Balance”: decimal }
        // Body example: { “id”: “LJ-4812-GL”, “vehicleType”: 2, “Balance”: 100 }
        // Response:
        // If body is invalid - Status Code: 400 Bad Request
        // If request is handled successfully
        // Status Code: 201 Created
        // Body schema: { “id”: string, “vehicleType”: int, "Balance": decimal }
        // Body example: { “id”: “LJ-4812-GL”, “vehicleType”: 2, "Balance": 100 }
        [HttpPost]
        public ActionResult CreateVehicle(Vehicle vehicle) {
            if (vehicle == null
                || Vehicle.IsBadId(vehicle.Id)
                || vehicle.Balance <= 0
                || (int)vehicle.VehicleType < 1
                || (int)vehicle.VehicleType > 4) {
                return BadRequest($"Bad request for vehicle: {vehicle}");
            }

            try {
                _parkingService.AddVehicle(vehicle);
            } catch (ArgumentException e) {
                return BadRequest($"Bad request. Error: " + e.Message.ToString());
            }

            var json = JsonConvert.SerializeObject(vehicle);
            return Created(Settings.HOST + "api/vehicles/" + vehicle.Id, json);
        }

        // DELETE api/vehicles/id(id - vehicle id of format “AA-0001-AA”)
        //
        // Response:
        // If id is invalid - Status Code: 400 Bad Request
        // If vehicle not found - Status Code: 404 Not Found
        // If request is handled successfully
        // Status Code: 204 No Content
        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(string id) {
            if (Vehicle.IsBadId(id)) {
                return BadRequest($"Bad request for vehicle: {id}");
            }

            var result = _parkingService.GetVehicle(id);
            if (result == null) {
                return NotFound($"Not found vehicle with id: {id}");
            }

            _parkingService.RemoveVehicle(id);

            return NoContent();
        }
    }
}