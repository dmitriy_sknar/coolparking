﻿using System;
using System.Linq;
using System.Text;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers {
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TransactionsController : Controller {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService) {
            _parkingService = parkingService;
        }

        // GET api/transactions/last
        //
        // Response:
        // If request is handled successfully
        // Status Code: 200 OK
        // Body schema: [{ “vehicleId”: string, “sum”: decimal, "transactionDate": DateTime }]
        // Body example: [{ “vehicleId”: "DG-3024-UB", “sum”: 3.5, "transactionDate": "2020-05-10T11:36:20.6395402+03:00"}]
        [HttpGet("last")]
        public IActionResult GetLastTransaction() {
            var result = _parkingService.GetLastParkingTransactions();
            if (result == null || !result.Any()) {
                // return NotFound("No transaction found");
                result = new TransactionInfo[0];
            }

            return Json(result);
        }

        // GET api/transactions/all
        //
        // Response:
        // If log file not found - Status Code: 404 Not Found
        // If request is handled successfully
        // Status Code: 200 OK
        // Body schema: string
        // Body example: “5/10/2020 11:21:20 AM: 3.50 money withdrawn from vehicle with Id = 'GP-5263-GC'.\n5/10/2020 11:21:25 AM: 3.50 money withdrawn from vehicle with Id = 'GP-5263-GC'.”
        [HttpGet("all")]
        public IActionResult GetAllTransactions() {
            try {
                var parkingTransactions = _parkingService.ReadFromLog();
                // string result = FormatTransactionsInfo(parkingTransactions);
                return Content(parkingTransactions);

            } catch (InvalidOperationException e) {
                return NotFound("No log file found. Internal error message: " + e.Message.ToString());
            }
        }

        private string FormatTransactionsInfo(TransactionInfo[] parkingTransactions) {
            StringBuilder strBuilder = new StringBuilder();
            foreach (var parkingTransaction in parkingTransactions) {
                strBuilder.Append(
                    $"{parkingTransaction.TransactionTime}: {parkingTransaction.Sum} money withdrawn from vehicle with Id = '{parkingTransaction.VehicleId}'.\n");
            }

            return strBuilder.ToString();
        }

        // PUT api/transactions/topUpVehicle
        //
        // Body schema: { “id”: string, “Sum”: decimal }
        // Body example: { “id”: “GP-5263-GC”, “Sum”: 100 }
        // Response:
        // If body is invalid - Status Code: 400 Bad Request
        // If vehicle not found - Status Code: 404 Not Found
        // If request is handled successfully
        // Status Code: 200 OK
        // Body schema: { “id”: string, “vehicleType”: int, "Balance": decimal }
        // Body example: { “id”: “GP-5263-GC”, “vehicleType”: 2, "Balance": 245.5 }
        [HttpPut("topUpVehicle")]
        public IActionResult TopUpVehicle(ParkingTopUpVehicle topUp) {
            if (topUp == null
                || Vehicle.IsBadId(topUp.Id)
                || topUp.Sum <= 0) {
                BadRequest($"Bad request to refill balance of vehicle: {topUp}");
            }

            var result = _parkingService.GetVehicle(topUp.Id);
            if (result == null) {
                return NotFound($"Cannot refill vehicle balance as vehicle was not found: {topUp}");
            }

            _parkingService.TopUpVehicle(topUp.Id, topUp.Sum);

            return Json(_parkingService.GetVehicle(topUp.Id));
        }
    }
}