﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers {
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ParkingController : Controller {
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService) {
            _parkingService = parkingService;
        }

        //GET api/parking/Balance
        // Response:
        // If request is handled successfully
        // Status Code: 200 OK
        //     Body schema: decimal
        //     Body example: 10.5
        [HttpGet("Balance")]
        public IActionResult Balance() {
            return Ok(_parkingService.GetBalance());
        }

        //GET api/parking/capacity
        //         Response:
        // If request is handled successfully
        // Status Code: 200 OK
        // Body schema: int
        // Body example: 10
        [HttpGet("capacity")]
        public IActionResult Capacity() {
            return Ok(_parkingService.GetCapacity());
        }

        // GET api/parking/freePlaces
        //
        // Response:
        // If request is handled successfully
        // Status Code: 200 OK
        // Body schema: int
        // Body example: 9
        [HttpGet("freePlaces")]
        public IActionResult FreePlaces() {
            return Ok(_parkingService.GetFreePlaces());
        }
    }
}
