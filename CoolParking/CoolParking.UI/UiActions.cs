﻿namespace CoolParking.UI {
    public enum UiActions {
        NoAction = 0,
        Balance = 1,
        EarnedMoneyForLastPeriod = 2,
        FreeAndOccupiedPlaces = 3,
        AllTransactionsForLastPeriod = 4,
        AllTransactionsIncludingLog = 5,
        AllParkedVehicles = 6,
        ParkVehicle = 7,
        RemoveVehicle = 8,
        ReplenishVehicleBalance = 9
    }
}