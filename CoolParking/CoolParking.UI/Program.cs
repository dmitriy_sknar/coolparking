﻿using CoolParking.BL.Interfaces;
using System;

namespace CoolParking.UI {
    class Program {
        static void Main(string[] args) {
            ConsoleUI consoleUi = new ConsoleUI();
            // consoleUi.Start();
            consoleUi.StartWeb();
        }
    }
}