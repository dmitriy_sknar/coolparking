﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Newtonsoft.Json;

namespace CoolParking.UI {
    public class ParkingWebClient : IParkingService {
        private HttpClient _client;

        public ParkingWebClient() {
            _client = new HttpClient();
        }

        public void Dispose() {
            _client.Dispose();
        }

        public decimal GetBalance() {
            Task<decimal> parkingBalance = GetBalanceAsync();
            return parkingBalance.Result;
        }

        private async Task<decimal> GetBalanceAsync() {
            var parkingBalance = await _client.GetStringAsync(Settings.HOST + "api/parking/Balance");
            Decimal.TryParse(parkingBalance.Replace(".", ","), out decimal balance);
            return balance;
        }

        public int GetCapacity() {
            Task<int> parkingCapacity = GetCapacityAsync();
            return parkingCapacity.Result;
        }

        private async Task<int> GetCapacityAsync() {
            var parkingCapacity = await _client.GetStringAsync(Settings.HOST + "api/parking/capacity");
            Int32.TryParse(parkingCapacity, out int capacity);
            return capacity;
        }

        public int GetFreePlaces() {
            Task<int> parkingFreePlaces = GetFreePlacesAsync();
            return parkingFreePlaces.Result;
        }

        private async Task<int> GetFreePlacesAsync() {
            var parkingFreePlaces = await _client.GetStringAsync(Settings.HOST + "api/parking/freePlaces");
            Int32.TryParse(parkingFreePlaces, out int freePlaces);
            return freePlaces;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles() {
            Task<List<Vehicle>> vehiclesTask = GetVehiclesAsync();
            var vehicles = vehiclesTask.Result;
            return new ReadOnlyCollection<Vehicle>(vehicles);
        }

        private async Task<List<Vehicle>> GetVehiclesAsync() {
            var parkingVehicles = await _client.GetStringAsync(Settings.HOST + "api/vehicles");
            return JsonConvert.DeserializeObject<List<Vehicle>>(parkingVehicles);
        }

        public Vehicle GetVehicle(string id) {
            Task<Vehicle> vehicle = GetVehicleAsync(id);
            return vehicle.Result;
        }

        private async Task<Vehicle> GetVehicleAsync(string id) {
            var parkingVehicle = await _client.GetStringAsync(Settings.HOST + $"api/vehicles/{id}");
            return JsonConvert.DeserializeObject<Vehicle>(parkingVehicle);
        }

        public void AddVehicle(Vehicle vehicle) {
            Task<HttpResponseMessage> response = AddVehicleAsync(vehicle);
            HttpResponseMessage responseResult = response.Result;

            if (responseResult.StatusCode != HttpStatusCode.Created) {
                throw new ArgumentException($"Vehicle cannot be parked. Error: {responseResult.Content}");
            }
        }

        private async Task<HttpResponseMessage> AddVehicleAsync(Vehicle vehicle) {
            var jsonObject = JsonConvert.SerializeObject(vehicle);
            var content = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            var result = await _client.PostAsync(Settings.HOST + "api/vehicles", content);
            return result;
        }

        public void RemoveVehicle(string vehicleId) {
            Task<HttpResponseMessage> response = RemoveVehicleAsync(vehicleId);
            HttpResponseMessage responseResult = response.Result;

            if (responseResult.StatusCode != HttpStatusCode.NoContent) {
                throw new ArgumentException($"Vehicle cannot be removed from parking. Error: {responseResult.Content}");
            }
        }

        private async Task<HttpResponseMessage> RemoveVehicleAsync(string vehicleId) {
            var result = await _client.DeleteAsync(Settings.HOST + $"api/vehicles/{vehicleId}");
            return result;
        }

        public void TopUpVehicle(string vehicleId, decimal sum) {
            Task<HttpResponseMessage> response = TopUpVehicleAsync(new ParkingTopUpVehicle(vehicleId, sum));
            HttpResponseMessage responseResult = response.Result;

            if (responseResult.StatusCode != HttpStatusCode.OK) {
                throw new ArgumentException($"Cannot top up vehicle. Error: {responseResult.Content}");
            }
        }

        private async Task<HttpResponseMessage> TopUpVehicleAsync(ParkingTopUpVehicle topUpVehicle) {
            var jsonObject = JsonConvert.SerializeObject(topUpVehicle);
            var content = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            var result = await _client.PutAsync(Settings.HOST + $"api/transactions/topUpVehicle", content);
            return result;
        }

        public TransactionInfo[] GetLastParkingTransactions() {
            try {
                Task<TransactionInfo[]> lastParkingTransactions = GetLastParkingTransactionsAsync();
                return lastParkingTransactions.Result;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                return new TransactionInfo[0];
            }
        }

        private async Task<TransactionInfo[]> GetLastParkingTransactionsAsync() {
            TransactionInfo[] transactions = new TransactionInfo[0];
            var responseResult = await _client.GetAsync(Settings.HOST + "api/transactions/last");
            if (responseResult.StatusCode != HttpStatusCode.OK) {
                return transactions;
            }

            var content = responseResult.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<TransactionInfo[]>(content);
        }

        public string ReadFromLog() {
            try {
                Task<string> allParkingTransactions = GetAllParkingTransactionsAsync();
                string result = allParkingTransactions.Result;
                return result;
            } catch (Exception e) {
                return $"Error occured: {e.Message}";
            }
        }

        private async Task<string> GetAllParkingTransactionsAsync() {
            var transactions = await _client.GetStringAsync(Settings.HOST + "api/transactions/all");
            return transactions;
        }
    }
}