﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.UI {
    public class ConsoleUI : IDisposable {
        private IParkingService _parkingService;
        private Dictionary<UiActions, string> ActionsDictionary;

        private void InitParking() {
            _parkingService = new ParkingService();
        }

        public void Dispose() {
            _parkingService.Dispose();
            ActionsDictionary.Clear();
        }

        public void Start() {
            InitParking();
            InitActions();
            GreatUser();
            Execute();
        }

        public void StartWeb() {
            InitParkingWebClient();
            InitActions();
            GreatUser();
            Execute();
        }

        private void InitParkingWebClient() {
            _parkingService = new ParkingWebClient();
        }

        private void Execute() {
            ShowActions();
            UiActions selectedAction = PromptUser();
            ExecuteAction(selectedAction);
            AskToContinue();
        }

        private void AskToContinue() {
            Console.WriteLine("\nDo you want to continue: yes (Y) \\ no (N)?");
            var key = Console.ReadKey().KeyChar;
            if (key == 'y' || key == 'Y') {
                Console.WriteLine();
                Execute();
            } else if (key == 'n' || key == 'N') {
                return;
            } else {
                Console.WriteLine("Please select right choice!");
                AskToContinue();
            }
        }

        private void ExecuteAction(UiActions selectedAction) {
            switch (selectedAction) {
                case UiActions.Balance:
                    Console.WriteLine($"Balance is:  {_parkingService.GetBalance():0.0}");
                    break;

                case UiActions.AllParkedVehicles:
                    Console.WriteLine("Parked vehicles: ");
                    var parkedVehicles = _parkingService.GetVehicles();
                    if (parkedVehicles.Count > 0) {
                        foreach (var vehicle in parkedVehicles) {
                            Console.WriteLine($"Vehicle Id:  {vehicle.Id}");
                        }
                    } else {
                        Console.WriteLine("No vehicle parked yet");
                    }

                    break;

                case UiActions.AllTransactionsForLastPeriod:
                    AllTransactionsForLastPeriod();
                    break;

                case UiActions.AllTransactionsIncludingLog:
                    AllTransactionsIncludingLog();
                    break;

                case UiActions.EarnedMoneyForLastPeriod:
                    Console.WriteLine($"Earned money: {_parkingService.GetBalance()}");

                    break;

                case UiActions.FreeAndOccupiedPlaces:
                    Console.WriteLine($"Free places: {_parkingService.GetFreePlaces()}. " +
                                      $"Occupied places number: {_parkingService.GetCapacity() - _parkingService.GetFreePlaces()}");

                    break;

                case UiActions.ParkVehicle:
                    ParkVehicle();
                    break;

                case UiActions.RemoveVehicle:
                    RemoveVehicle();
                    break;

                case UiActions.ReplenishVehicleBalance:
                    ReplenishVehicleBalance();
                    break;

                default:
                    Console.WriteLine("Such action is not supported");
                    break;
            }
        }

        private void AllTransactionsForLastPeriod() {
            Console.WriteLine("Last transactions: ");
            foreach (var transactionInfo in _parkingService.GetLastParkingTransactions()) {
                Console.WriteLine(transactionInfo.ToString());
            }
        }

        private void AllTransactionsIncludingLog() {
            // AllTransactionsForLastPeriod();

            Console.WriteLine("\nTransactions in log: \n");
            try {
               Console.WriteLine(_parkingService.ReadFromLog());
            } catch (Exception e) {
                e.Message.ToString();
            }
        }

        private void ReplenishVehicleBalance() {
            Console.WriteLine("Please enter vehicle id to replenish in format: XX-NNNN-XX. There X is a letter, N is a number");
            var id = Console.ReadLine();
            if (Vehicle.IsBadId(id)) {
                Console.WriteLine("Vehicle Id is not valid. Please reenter");
                RemoveVehicle();
            }

            Console.WriteLine("Please enter replenishment amount");
            var input = Console.ReadLine();
            if (!Decimal.TryParse(input, out decimal balance) || balance <= 0) {
                Console.WriteLine("Vehicle Id is not valid. Please reenter");
                ReplenishVehicleBalance();
            }

            try {
                _parkingService.TopUpVehicle(id, balance);
            } catch (ArgumentException e) {
                e.Message.ToString();
            }
        }

        private void RemoveVehicle() {
            Console.WriteLine("Please enter vehicle id to remove in format: XX-NNNN-XX. There X is a letter, N is a number");
            var id = Console.ReadLine();
            if (Vehicle.IsBadId(id)) {
                Console.WriteLine("Vehicle Id is not valid. Please reenter");
                RemoveVehicle();
            }

            try {
                _parkingService.RemoveVehicle(id);
            } catch (InvalidOperationException e) {
                e.Message.ToString();
            } catch (ArgumentException e) {
                e.Message.ToString();
            }
        }

        private void ParkVehicle() {
            Console.WriteLine("Please enter vehicle id in format: XX-NNNN-XX. There X is a letter, N is a number");
            var id = Console.ReadLine();
            if (Vehicle.IsBadId(id)) {
                Console.WriteLine("Vehicle Id is not valid. Please reenter");
                ParkVehicle();
            }

            Console.WriteLine("Please enter vehicle balance");
            var input = Console.ReadLine();
            if (!Decimal.TryParse(input, out decimal balance) || balance <= 0) {
                Console.WriteLine("Vehicle Id is not valid. Please reenter");
                ParkVehicle();
            }

            Console.WriteLine("Please enter vehicle Type: Bus - 1, Truck - 2, PassengerCar - 3, Motorcycle - 4");
            var typeInput = Console.ReadLine();
            if (!Int32.TryParse(typeInput, out int type) || type <= 0 || type > 4) {
                Console.WriteLine("Vehicle type is not valid. Please reenter");
                ParkVehicle();
            }

            try {
                _parkingService.AddVehicle(new Vehicle(id, (VehicleType)type, balance));
            } catch (InvalidOperationException e) {
                e.Message.ToString();
            } catch (ArgumentException e) {
                e.Message.ToString();
            }
        }

        private void ShowActions() {
            Console.WriteLine("\nCoolParking actions:");
            for (int i = 0; i < ActionsDictionary.Count; i++) {
                Console.WriteLine($"{i + 1} - {ActionsDictionary.ElementAt(i).Value}");
            }
        }

        private void GreatUser() {
            Console.WriteLine("CoolParking greats you!");
        }

        private UiActions PromptUser() {
            Console.WriteLine("\nExecute an action:");
            var input = Console.ReadLine();
            if (Int32.TryParse(input, out int actionId) && actionId > 0 && actionId <= ActionsDictionary.Count)
                return (UiActions)actionId;
            else {
                Console.WriteLine("Please enter valid action.");
                PromptUser();
            }

            return UiActions.NoAction;
        }

        private void InitActions() {
            ActionsDictionary = new Dictionary<UiActions, string> {
                { UiActions.Balance, "Show CoolParking balance" },
                { UiActions.EarnedMoneyForLastPeriod, "Show earned money for last period" },
                { UiActions.FreeAndOccupiedPlaces, "Show free and occupied places" },
                { UiActions.AllTransactionsForLastPeriod, "Show all transactions for last period" },
                { UiActions.AllTransactionsIncludingLog, "Show all transactions including already logged" },
                { UiActions.AllParkedVehicles, "Show all parked vehicles" },
                { UiActions.ParkVehicle, "Park the vehicle" },
                { UiActions.RemoveVehicle, "Remove the vehicle from parking" },
                { UiActions.ReplenishVehicleBalance, "Replenish vehicle's balance" }
            };
        }
    }
}